const findBmwAudiCars = (inventory) => {
    let newArr = [];
    for (let i=0; i<inventory.length-1; i++) {
        if (inventory[i]['car_make']==='BMW' || inventory[i]['car_make']==='Audi'){
            newArr.push(inventory[i]);
        }
    }

    return newArr;
}


module.exports =  { findBmwAudiCars } ;